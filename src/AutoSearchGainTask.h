// ============================================================================
//
// = CONTEXT
//    TANGO Project - Magnet Manager Class
//
// = FILENAME
//    AutoSearchGainTask.hpp
//
// = AUTHOR
//    A. Buteau
// ============================================================================

#ifndef _AUTOSEARCH_GAIN_TASK_H_
#define _AUTOSEARCH_GAIN_TASK_H_

#include <tango.h>
#include <string>
#include <yat4tango/DeviceTask.h>
#include <FemtoCurrentAmplifier.h>


/**
 *  \brief Visible class from the DServer
 *
 *  \author Xavier Elattaoui
 *  \date 08-2010
 */
namespace FemtoCurrentAmplifier_ns
{

class AutoSearchGainTask : public yat4tango::DeviceTask
{
public:

	/**
	*  \brief Initialization.
	*/
	AutoSearchGainTask ( Tango::DeviceImpl * host_device);

	/**
	*  \brief Release resources.
	*/
	virtual ~AutoSearchGainTask ();
	void search_gain() ;
	
protected:
  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg)
		throw (Tango::DevFailed);

private:
  //- the host device
    //- the host device
  class FemtoCurrentAmplifier* _host_dev;
  void  autosearch_gain();

};

} //- end namespace

#endif //_AUTOSEARCH_GAIN_TASK_H_
