static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/Femto/src/ClassFactory.cpp,v 1.2 2011-02-15 12:15:12 buteau Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible to create
//               all class singletin for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: buteau $
//
// $Revision: 1.2 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <FemtoCurrentAmplifierClass.h>

/**
 *	Create FemtoCurrentAmplifierClass singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{

	add_class(FemtoCurrentAmplifier_ns::FemtoCurrentAmplifierClass::init("FemtoCurrentAmplifier"));

}
