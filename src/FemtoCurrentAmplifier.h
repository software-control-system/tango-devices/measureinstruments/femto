//=============================================================================
//
// file :        FemtoCurrentAmplifier.h
//
// description : Include for the FemtoCurrentAmplifier class.
//
// project :	Femto Low-Noise Current Amplifier
//
// $Author: buteau $
//
// $Revision: 1.15 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _FEMTOCURRENTAMPLIFIER_H
#define _FEMTOCURRENTAMPLIFIER_H

#include <tango.h>
#include <TangoExceptionsHelper.h>
#include <DeviceProxyHelper.h>
#include <Xstring.h>
#include <yat4tango/DeviceTask.h>

//using namespace Tango;

/**
 * @author	$Author: buteau $
 * @version	$Revision: 1.15 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------

const int MAX_STRING_LENGTH = 32;
const int MAX_GAIN_INDEX_FEMTO_100=6;
const int MAX_GAIN_INDEX_FEMTO_200=7;
const int MAX_GAIN_INDEX_FEMTO_300=10;

namespace FemtoCurrentAmplifier_ns
{

/**
 * Class Description:
 * Device which controls the Femto Low-Noise Current Amplifier.<BR>
 *	It uses the SingleShotDIO generic Device to communicate with the Femto.
 */

/*
 *	Device States Description:
*  Tango::ON :       Normal operation
*  Tango::ALARM :    Femto is on Overload - or overload is not responding
*  Tango::RUNNING :  Automatically Searching the good Gain.
*  Tango::FAULT :    happend when :
 *                    - Tango:exception occured during creation of the device proxy
 *                    - memory allocation problem
 */


class FemtoCurrentAmplifier: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------

	short get_max_gain_index() {return MAX_GAIN_INDEX;};
	void write_gain(short gain);
	bool get_overload() ;

	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevShort	attr_gain_write;
		Tango::DevBoolean	attr_coupling_write;
		Tango::DevBoolean	attr_gainMode_write;
		Tango::DevShort	*attr_overload_read;
		Tango::DevDouble	*attr_outputSignal_read;
		Tango::DevString	*attr_gainSelected_read;
		Tango::DevShort	attr_upperBWLimit_write;
		Tango::DevDouble	*attr_gainSelectedNum_read;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	Name of the DIO board device server
 */
	string	dIOBoardName;
/**
 *	Line of the Coupling (AC/DC).
 */
	string	couplingLine;
/**
 *	Line of the Overload
 */
	string	overloadLine;
/**
 *	Line of the Gain Mode (HS/LN).
 */
	string	gainModeLine;
/**
 *	Line of the Output Signal
 */
	string	outputSignalLine;
/**
 *	Line of the LSB Gain
 */
	string	lSBGainLine;
/**
 *	Line of the Mid SB Gain
 */
	string	midSBGainLine;
/**
 *	Line of the MSB Gain
 */
	string	mSBGainLine;
/**
 *	Type of the Femto Current Amplifier: (Default = 2)<BR>
 *	1 -> DHPCA-100<BR>
 *	2 -> DLPCA-200
 */
	Tango::DevShort	femtoType;
/**
 *	Line of the Upper BandWidth Limit 10 MHz.
 */
	string	uBWL10MHzLine;
/**
 *	Line of the Upper BandWidth Limit 1 MHz.
 */
	string	uBWL1MHzLine;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	FemtoCurrentAmplifier(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	FemtoCurrentAmplifier(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	FemtoCurrentAmplifier(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~FemtoCurrentAmplifier() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name FemtoCurrentAmplifier methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for gain acquisition result.
 */
	virtual void read_gain(Tango::Attribute &attr);
/**
 *	Write gain attribute values to hardware.
 */
	virtual void write_gain(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for coupling acquisition result.
 */
	virtual void read_coupling(Tango::Attribute &attr);
/**
 *	Write coupling attribute values to hardware.
 */
	virtual void write_coupling(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for gainMode acquisition result.
 */
	virtual void read_gainMode(Tango::Attribute &attr);
/**
 *	Write gainMode attribute values to hardware.
 */
	virtual void write_gainMode(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for overload acquisition result.
 */
	virtual void read_overload(Tango::Attribute &attr);
/**
 *	Extract real attribute values for outputSignal acquisition result.
 */
	virtual void read_outputSignal(Tango::Attribute &attr);
/**
 *	Extract real attribute values for gainSelected acquisition result.
 */
	virtual void read_gainSelected(Tango::Attribute &attr);
/**
 *	Extract real attribute values for upperBWLimit acquisition result.
 */
	virtual void read_upperBWLimit(Tango::Attribute &attr);
/**
 *	Write upperBWLimit attribute values to hardware.
 */
	virtual void write_upperBWLimit(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for gainSelectedNum acquisition result.
 */
	virtual void read_gainSelectedNum(Tango::Attribute &attr);
/**
 *	Read/Write allowed for gain attribute.
 */
	virtual bool is_gain_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for coupling attribute.
 */
	virtual bool is_coupling_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for gainMode attribute.
 */
	virtual bool is_gainMode_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for overload attribute.
 */
	virtual bool is_overload_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for outputSignal attribute.
 */
	virtual bool is_outputSignal_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for gainSelected attribute.
 */
	virtual bool is_gainSelected_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for upperBWLimit attribute.
 */
	virtual bool is_upperBWLimit_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for gainSelectedNum attribute.
 */
	virtual bool is_gainSelectedNum_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for IncreaseGain command.
 */
	virtual bool is_IncreaseGain_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for DecreaseGain command.
 */
	virtual bool is_DecreaseGain_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for AutoSearchGain command.
 */
	virtual bool is_AutoSearchGain_allowed(const CORBA::Any &any);
/**
 * Increase the Gain (max = 6).
 *	@exception DevFailed
 */
	void	increase_gain();
/**
 * Decrease the Gain (min = 0).
 *	@exception DevFailed
 */
	void	decrease_gain();
/**
 * Automatically search Gain:
 *	Set 1st Gain
 *	Check if Overload
 *	....
 *	@exception DevFailed
 */
	void	auto_search_gain();
/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	

/**
 *	Read the device properties from database
 */
	void creates_dio_proxy();

private :	
	//	Add your own data members here
	//-----------------------------------------
	void set_gain_on_dio_board(short gain_value);
	string get_gain_on_dio_board();

	short lsb,midsb,msb,calc_gain;
    double gain_value_of_the_proxy;

	Tango::DeviceProxyHelper *dio_board_proxy;

	static string femto_100_LN_gains[MAX_GAIN_INDEX_FEMTO_100];
	static double femto_100_LN_gains_num[MAX_GAIN_INDEX_FEMTO_100];

	static string femto_100_HS_gains[MAX_GAIN_INDEX_FEMTO_100];
	static double femto_100_HS_gains_num[MAX_GAIN_INDEX_FEMTO_100];

	static string femto_200_LN_gains[MAX_GAIN_INDEX_FEMTO_200];
	static double femto_200_LN_gains_num[MAX_GAIN_INDEX_FEMTO_200];

	static string femto_200_HS_gains[MAX_GAIN_INDEX_FEMTO_200];
	static double femto_200_HS_gains_num[MAX_GAIN_INDEX_FEMTO_200];

	static string femto_300_gains[MAX_GAIN_INDEX_FEMTO_300];
	static double femto_300_gains_num[MAX_GAIN_INDEX_FEMTO_300];

	short MAX_GAIN_INDEX;

	bool is_dio_proxy_created;
	class AutoSearchGainTask* _auto_search_task;
    yat::Mutex _state_mutex;
    yat::Mutex _status_mutex;
    yat::Mutex _get_overload_mutex;
  yat::Mutex _write_gain_mutex;

	public :
	void internal_set_state(Tango::DevState new_state)
	{ 	yat::AutoMutex<> guard(this->_state_mutex); 
		// call Tango method
		set_state(new_state);
	};
	void internal_set_status(std::string new_status)
	{ 	yat::AutoMutex<> guard(this->_status_mutex); 
		// call Tango method
		set_status(new_status);
	};

};




}	// namespace

#endif	// _FEMTOCURRENTAMPLIFIER_H
