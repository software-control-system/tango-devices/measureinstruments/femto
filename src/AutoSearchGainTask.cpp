// ============================================================================
//
// = CONTEXT
//    TANGO Project - Magnet Manager Class
//
// = FILENAME
//    AutoSearchGainTask.cpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#include <cmath>
#include "AutoSearchGainTask.h"

// ----------------------------------------------------------------------------
const size_t kPERIODIC_TMOUT_MS	= 30000;	//- task period = in ms

const size_t AUTOSEARCH_TMOUT_MS = 10000;	//- task period = in ms
//---------------------------------------------------------
//- the YAT user messages : switch magnet sequence
//const size_t SWICTH_MAGNET_SEQ = yat::FIRST_USER_MSG + 1;
//---------------------------------------------------------
//- the YAT user messages
const size_t AUTO_SEARCH_GAIN = yat::FIRST_USER_MSG + 1;


namespace FemtoCurrentAmplifier_ns
{
// ============================================================================
// AutoSearchGainTask::AutoSearchGainTask
// ============================================================================
AutoSearchGainTask::AutoSearchGainTask (Tango::DeviceImpl* host_device)
: yat4tango::DeviceTask(host_device)
{
  DEBUG_STREAM << "CTOR : AutoSearchGainTask::AutoSearchGainTask " << std::endl;

	_host_dev= (FemtoCurrentAmplifier* )host_device;
        this->enable_timeout_msg(false);
  	// no need to get periodic activity
        this->enable_periodic_msg(false);
        this->set_periodic_msg_period(kPERIODIC_TMOUT_MS);
}

// ============================================================================
// AutoSearchGainTask::~AutoSearchGainTask
// ============================================================================
AutoSearchGainTask::~AutoSearchGainTask (void)
{
  DEBUG_STREAM << "DTOR : AutoSearchGainTask::~AutoSearchGainTask " << std::endl;
}

//-----------------------------------------------
//- the user core of the Task -------------------
void AutoSearchGainTask::process_message (yat::Message& _msg)
throw (Tango::DevFailed)
{
  //- The DeviceTask's lock_ -------------
  DEBUG_STREAM << "AutoSearchGainTask::process_message::receiving msg " << _msg.type() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
    //- THREAD_INIT =======================
  case yat::TASK_INIT:
  {
    DEBUG_STREAM << "AutoSearchGainTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;
   }
  break;
  //- TASK_EXIT =======================
  case yat::TASK_EXIT:
  {
    //- "release" code goes here
    DEBUG_STREAM << "AutoSearchGainTask::handle_message handling TASK_EXIT thread is quitting ..." << std::endl;
  }
  break;
  //- TASK_PERIODIC ===================
  case yat::TASK_PERIODIC:
  {
    DEBUG_STREAM << "AutoSearchGainTask::handle_message handling TASK_PERIODIC msg ..." << std::endl;
  }
  break;
  //- WRITE_SET_POINT =======================
  case AUTO_SEARCH_GAIN:
  {
     DEBUG_STREAM << "AutoSearchGainTask::handle_message handling AUTO_SEARCH_GAIN msg ..." << std::endl;    

    this->autosearch_gain();
   }
  break;
  } //- switch (_msg.type())
} //- AutoSearchGainTask::process_message

//-----------------------------------------------
//- Internal Method to search the gain 
 //-----------------------------------------------

void AutoSearchGainTask::autosearch_gain (void)
{
   INFO_STREAM << "AutoSearchGainTask::autosearch_gain AUTO_SEARCH_GAIN msg ..." << std::endl;    

	//- Start the Loop on Gains
	for(int i = _host_dev->get_max_gain_index();i > -1; i--)
	{
		//- set the gain
                 INFO_STREAM << "AutoSearchGainTask::autosearch_gain trying to write gain = " << i << std::endl;    
		_host_dev->write_gain((short)i);
		omni_thread::sleep(1);
		//- Check if overload happen: 1->overload , 0->not overload
		if(_host_dev->get_overload() == 0)
		{
			INFO_STREAM << " No Overload " ;
			break;
		}
		else
			INFO_STREAM << "Overload still present ==> try decreasing gain" << endl;

	}

}

//-----------------------------------------------
//- external Method to search the gain 
 //-----------------------------------------------

void AutoSearchGainTask::search_gain (void)
{
     DEBUG_STREAM << "AutoSearchGainTask::search_gain starting..." << std::endl;    

  //- create and post msg
  yat::Message* msg = new yat::Message(AUTO_SEARCH_GAIN, MAX_USER_PRIORITY,true);
  if ( !msg )
  {
    ERROR_STREAM << "AutoSearchGainTask::search_gain yat::Message allocation failed." << std::endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "ABC1260Task::get_battery_charge");
  }

  //- wait till the message is processed !!
  this->post(msg, AUTOSEARCH_TMOUT_MS);
 }
  
  

} //- end namespace

